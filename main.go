package main

import (
	"bitbucket.org/tampajohn/authorize"
	"github.com/codegangsta/negroni"
	"github.com/gorilla/mux"
	"io/ioutil"
	"net/http"
	"os"
)

type product struct {
	Sku  string `bson:"s"`
	Name string `bson:"n"`
}

func HomeHandler(w http.ResponseWriter, r *http.Request) {
	w.Write([]byte("Home"))
}

func SecureHandler(w http.ResponseWriter, r *http.Request) {
	w.Write([]byte("Secured"))
}

func LoginHandler(w http.ResponseWriter, r *http.Request) {
	privateKey, _ := ioutil.ReadFile(os.Getenv("SIGNING_KEY_PATH"))
	claims := map[string]string{
		"id":   "setup",
		"name": "john",
		"Role": "Customer",
	}
	//fmt.Println(claims)
	authorize.WriteToken(w, authorize.Options{
		SigningKey:    privateKey,
		SigningMethod: "RS256",
		Claims:        claims,
	})
}

func main() {
	publicKey, _ := ioutil.ReadFile(os.Getenv("DECRYPT_KEY_PATH"))
	router := mux.NewRouter()
	router.HandleFunc("/login", LoginHandler)

	apiRoutes := mux.NewRouter()
	apiRoutes.HandleFunc("/api", SecureHandler)

	router.PathPrefix("/api").Handler(negroni.New(
		authorize.NewAuthorize(authorize.Options{
			PublicKey: publicKey,
			Claims: map[string]string{
				"Role": "Admin",
			},
		}),
		negroni.Wrap(apiRoutes),
	))

	n := negroni.New(
		negroni.NewLogger(),
		negroni.NewStatic(http.Dir("public")),
	)
	n.UseHandler(router)
	n.Run(":3000")
}
